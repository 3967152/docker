FROM alpine

LABEL maintainer="Rui Gomes"

COPY cv /opt/docker/cv

WORKDIR /opt/docker/

CMD ["cat","/opt/docker/cv"]
